import React from 'react'

export const PININ_VOWEL_REGEX = /[aeiou](\d)/;

export function vowelToPinin(vowelAndTone, tone) {
  if (tone < 0 || tone > 4) {
    return vowelAndTone
  }
  const map = new Map([
    ['a1', 'ā'], ['a2', 'á'], ['a3', 'ǎ'], ['a4', 'à'],
    ['o1', 'ō'], ['o2', 'ó'], ['o3', 'ǒ'], ['o4', 'ò'],
    ['e1', 'ē'], ['e2', 'é'], ['e3', 'ě'], ['e4', 'è'],
    ['u1', 'ū'], ['u2', 'ú'], ['u3', 'ǔ'], ['u4', 'ù'],
    ['i1', 'ī'], ['i2', 'í'], ['i3', 'ǐ'], ['i4', 'ì'],
  ]);
  return map.get(vowelAndTone)
}

export function wordsToPinin(words) {
  return words.map(w => w.replace(PININ_VOWEL_REGEX, vowelToPinin))
}

export function validateInput(props) {
  return <>
    <p className='pinin' style={{color: (props.input === props.phrase.pinin ? 'green' : 'red')}}>{props.input || 'Empty'}</p>
    <p className='hieroglyph'>{props.phrase.hieroglyph}</p>
    <p className='pinin'>{props.phrase.pinin}</p>
    <p className='translation'>{props.phrase.translate}</p>
  </>
}
