import React, { useState } from 'react';
import Sound from 'react-sound';
import { getSoundUrl } from '../SoundAdapter';
import _ from 'lodash';
import './Card.css';
import { Container, Row, Col } from 'react-bootstrap';


import Audio from './exersises/Audio'
import Pinin from './exersises/Pinin'
import Translation from './exersises/Translation'
import { samples } from '../samples'

function Card(props) {

  const [phrase, setPhrase] = useState(_.sample(samples));
  const [audioStatus, setAudioStatus] = useState(Sound.status.PLAYING);
  const [input, setInput] = useState('');
  const [isAnswerShown, setIsAnswerShown] = useState(false);

  const nextPhrase = () => {
    setInput('')
    setPhrase(_.sample(samples));
    setIsAnswerShown(false);
    setAudioStatus(Sound.status.PLAYING)
  };

  const checkPhrase = () => {
    setIsAnswerShown(true);
    setAudioStatus(Sound.status.PLAYING)
  };

  const state = {
    phrase: phrase,
    setPhrase: setPhrase,
    input: input, setInput: setInput,
    isAnswerShown: isAnswerShown,
    setIsAnswerShown: setIsAnswerShown,
    checkPhrase: checkPhrase,
  };

  return (
    <Container>
      <Row>
        <Col xs={12} md={10} className='card__container'>
          <div className='card__content'>

            {props.inProgress === 'Translation' && <Translation {...state} />}
            {props.inProgress === 'Pinin' && <Pinin {...state} />}
            {props.inProgress === 'Audio' && <Audio {...state} />}
            <Sound
              url={getSoundUrl(phrase)}
              playStatus={audioStatus}
              onLoading={() => console.log('loading')}
              onFinishedPlaying={() => setAudioStatus(Sound.status.STOPPED)}
            />
          </div>
          <div className='buttons'>

            <button type='button' className='repeat' onClick={() => setAudioStatus(Sound.status.PLAYING)} />

            {isAnswerShown ?
              <button className='next' onClick={nextPhrase} autoFocus={true}>Next</button>
              :
              <button className='check' onClick={() => setIsAnswerShown(true)}>Check</button>
            }
          </div>
        </Col>
      </Row>
    </Container>
  )
}

export default Card;
