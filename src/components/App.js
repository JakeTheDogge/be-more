import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Card from './Card'
import "./App.css"

function App() {
  const [inProgress, setInProgress] = useState('Pinin');
  return (
    <>
      <div className='navbar'>
        <div className='items'>
          <button
            className={inProgress === 'Translation' ? 'pressed' : 'notPressed'}
            onClick={() => { setInProgress('Translation') }}>
            Translation
          </button>
          <button
            className={inProgress === 'Pinin' ? 'pressed' : 'pininNavBar'}
            onClick={() => { setInProgress('Pinin') }}>
            Pinin
          </button>
          <button
            className={inProgress === 'Audio' ? 'pressed' : 'notPressed'}
            onClick={() => { setInProgress('Audio') }}>
            Audio
          </button>
        </div>
      </div>
      {inProgress && <Card inProgress={inProgress} />}
    </>
  );
}

export default App;
