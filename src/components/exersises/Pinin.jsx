import React from 'react';
import PininInput from './PininInput'
import { validateInput } from "../../helpers/pinin";


function Pinin(props) {
  return (
    <>
      <div className='words'>
        {props.isAnswerShown ?
          <>{validateInput(props)}</>
          :
          <>
          <p className='hieroglyph'>{props.phrase.hieroglyph}</p>
          <PininInput input={props.input} setInput={props.setInput} setIsAnswerShown={props.setIsAnswerShown} />
          </>
        }
      </div>
    </>
  )
}

export default Pinin
