import React from 'react';
import { vowelToPinin, PININ_VOWEL_REGEX}  from '../../helpers/pinin'


function PininInput(props) {
  return (
    <input
      className='pininInput'
      onChange={e => props.setInput(e.target.value.replace(PININ_VOWEL_REGEX, vowelToPinin))}
      onKeyPress={e => { if (e.key === 'Enter') { props.setIsAnswerShown(true) } }}
      value={props.input}
      autoFocus={true}
    />
  )
}

export default PininInput
